package update.bitbucket;

import play.Configuration;
import play.Logger;
import play.Play;
import play.libs.WS;
import play.libs.WS.Response;
import play.mvc.Http;
import update.BetterrevActor;
import update.pullrequest.ImportPullRequestsEvent;
import com.fasterxml.jackson.databind.JsonNode;

/**
 * Polls the Bitbucket API and delegates to the PullRequestImporter class
 */
public class BitbucketPoller extends BetterrevActor {

    private static final int ONE_SECOND_TIMEOUT = 1000;
    private static final String API_URL = "https://bitbucket.org/api/2.0/repositories/%s/%s/pullrequests/";

    @Override
    public void onReceive(Object message) {
        if (message instanceof PollBitbucketEvent) {
            Logger.debug("PollBitbucketEvent received.");

            Configuration configuration = Play.application().configuration();
            String owner = configuration.getString("owner");
            for (String projectToken : configuration.getString("projects").split(" ")) {
                String project = projectToken.trim();
                Logger.info(String.format("Polling bitbucket with owner '%s' and project '%s'", owner, project));

                Response response = WS.url(String.format(API_URL, owner, project)).get().get(ONE_SECOND_TIMEOUT);
                JsonNode responseJson = response.asJson();
                if ((response.getStatus() != Http.Status.OK) || responseJson == null) {
                    Logger.error("Bitbucket did not return a valid response on the current execution of run()...");
                } else {
                    eventStream().publish(new ImportPullRequestsEvent(responseJson, project));
                }
            }
        }
    }
}
