Betterrev
=========

A friendly, hosted wrapper around OpenJDK Contributions and 'webrevs'. See the [Wiki](https://bitbucket.org/adoptopenjdk/betterrev/wiki/Home) for details on the [Grand Plan](https://bitbucket.org/adoptopenjdk/betterrev/wiki/Test%20and%20CI),[Workflow](https://bitbucket.org/adoptopenjdk/betterrev/wiki/Work%20Flow) and more.

If you already have the Betterrev source code downloaded and configured then please scroll down for more details on configuring your favourite IDE, otherwise please follow the 'Getting started...' instructions directly below.

Getting started for developers
------------------------------

* [Download and install play 2.2.2](http://www.playframework.com/documentation/2.2.2/Installing)    

* Install [Git](http://git-scm.com/downloads) for your operating system. 

* Install [Mercurial](http://mercurial.selenic.com/) for you operating system.

* [Fork the betterrev repository](https://bitbucket.org/adoptopenjdk/betterrev/fork)    
**WARNING** Untick the **issues** and **wiki** checkboxes, you do not want those!

* Clone your fork onto your local file system. You have two options to choose from:
* If you are using bitbucket git via http use the following command:

`git clone https://<your username>@bitbucket.org/<your username>/betterrev.git betterrev_project`

* -OR- If you are using bitbucket git via ssh and have created ssh keys on your local machine use the following command (for more info on this method of interacting with bitbucket please read [this bitbucket wiki article](https://confluence.atlassian.com/display/BITBUCKET/Set+up+SSH+for+Git))

`git clone git@bitbucket.org:<your_username>/betterrev.git betterrev_project`

* Clone the adopt repository into the betterrev_project/betterrev directory:

```
cd betterrev_project/betterrev 
hg clone https://bitbucket.org/adoptopenjdk/adopt adopt
```

* From within the betterrev_project/betterrev directory, clone the jdk9 repository: 

```
cd adopt
hg clone https://bitbucket.org/adoptopenjdk/jdk9 jdk9
```
* Get the jdk9 sources:
    * `$ cd jdk9` 
    * `$ chmod u+x get_source.sh`
    * `$ ./get_source.sh`

```    
Note that betterrev/adopt/jdk9/get_source.sh does not currently work due to 
the structuring of the adoptopenjdk repositories. 
```

* Your final directory structure should look something like the following. There may be directories missing (e.g., bin, logs, target), but the important dirs that you should check are present and in the correct location are **betterrev**, **adopt**, **jdk9**:

```
betterrev_project\
    readme.md
    LICENSE
    betterrev\
        adopt\
            jdk9\  
        app\
        bin\
        conf\
        logs\
        project\
        public\
        target\
        test\
```

* Go to the betterrev_project/betterrev directory and get all of the source and javadoc for the dependencies via:

`play updateClassifiers`

* Go to the betterrev_project/betterrev directory and launch the Play sbt interactive console by typing:

`play`

* From the Play Console you can execute the following commands (from within the $SOURCE/betterrev/betterrev/ folder)
    * `[betterrev] $ compile`
    * `[betterrev] $ test`
    * `[betterrev] $ run`
* The commands should be self-explanatory, and so go ahead and run the Betterrev application
* Launch [Betterrev](http://localhost:9000/). 
    * You may see an error page in your browser (and a corresponding stacktrace in the play terminal window) stating something like **Database 'default' needs evolution!**, but this is normal, and you should click the 'Apply this script now!'. Play uses the 'ebean' framework to manage/migrate the database schema (much like the frameworks 'liquibase' or 'flyway'), and this error is simply stating that your database schema needs to be created or updated.

Developing using IntelliJ IDEA
------------------------------

See [IntelliJ Instructions](https://bitbucket.org/adoptopenjdk/betterrev/wiki/IntelliJ%20IDEA%20Instructions)

Developing using Eclipse
------------------------

See [Eclipse Instructions](https://bitbucket.org/adoptopenjdk/betterrev/wiki/Eclipse%20Instructions)